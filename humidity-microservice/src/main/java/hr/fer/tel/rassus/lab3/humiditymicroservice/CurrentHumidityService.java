package hr.fer.tel.rassus.lab3.humiditymicroservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrentHumidityService {
    @Autowired
    private HumidityRepository humidityRepository;

    public HumidityReading getCurrentHumidity() {
    	long id = (System.currentTimeMillis() % 100) + 1;
    	
        HumidityReading humidityReading = this.humidityRepository.getById(id);
        
        return humidityReading;
    }
}