package hr.fer.tel.rassus.lab3.humiditymicroservice;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class HumidityReading {
    @Id
    private long id;
    
    private final String name = "Humidity";
    private final String unit = "%";
    
    private String value;

    public HumidityReading(String value) {
        this.value = value;
    }
    
    public HumidityReading() {
    	
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public String getName() {
    	return name;
    }
    
    public String getUnit() {
    	return unit;
    }
    
    public String getValue() {
    	return value;
    }
    
    public void setValue(String value) {
    	this.value = value;
    }

    @Override
    public String toString() {
        return "Humidity reading: [ "+
                "id: " + id + ", " +
                "name: " + name + ", " +
                "unit: " + unit + ", " +
                "value: " + value +
                " ]";
    }

}
