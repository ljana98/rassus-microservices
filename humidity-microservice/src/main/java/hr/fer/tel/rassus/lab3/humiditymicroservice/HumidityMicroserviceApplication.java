package hr.fer.tel.rassus.lab3.humiditymicroservice;

import java.io.FileReader;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import com.opencsv.bean.CsvToBeanBuilder;

@EnableDiscoveryClient
@SpringBootApplication
public class HumidityMicroserviceApplication implements CommandLineRunner {
	
	@Autowired
	private HumidityRepository humidityRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(HumidityMicroserviceApplication.class, args);
	} 

	@Override
	public void run(String... args) throws Exception {
		List<Reading> readings = new CsvToBeanBuilder<Reading>(new FileReader("../readings.csv"))
			       .withType(Reading.class).build().parse();
		
		long l = 0;
		
		for (Reading reading: readings) {
				String humidity = reading.getHumidity();
				
				HumidityReading humidityReading = new HumidityReading(humidity);	
				
				humidityReading.setId(l++);
				
				humidityRepository.save(humidityReading);
				
				System.out.println(humidityReading);
		}
	}

}
