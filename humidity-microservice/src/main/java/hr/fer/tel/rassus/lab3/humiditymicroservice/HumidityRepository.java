package hr.fer.tel.rassus.lab3.humiditymicroservice;

import org.springframework.data.jpa.repository.JpaRepository;

public interface HumidityRepository extends JpaRepository<HumidityReading, Long> {

}
