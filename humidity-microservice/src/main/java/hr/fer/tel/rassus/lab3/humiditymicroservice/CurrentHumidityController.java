package hr.fer.tel.rassus.lab3.humiditymicroservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class CurrentHumidityController {
	CurrentHumidityService currentHumidityService;

    @Autowired
    public CurrentHumidityController(CurrentHumidityService currentHumidityService) {
        this.currentHumidityService = currentHumidityService;
    }

    @GetMapping(value="/current-humidity")
    public ResponseEntity<HumidityReading> getCurrentHumidity() {
    	HumidityReading currentHumidity = this.currentHumidityService.getCurrentHumidity();

        return currentHumidity != null ?
                ResponseEntity.ok().body(currentHumidity)
                	: ResponseEntity.noContent().build();
    }
}

