package hr.fer.tel.rassus.lab3.aggregatormicroservice;


import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AggregatorController {
    @Autowired
    private ReadingService readingsService;

    @Value("${config.temperatureUnit}")
    private String temperatureUnit;

    @GetMapping("/reading")
    public ResponseEntity<Set<Reading>> getCurrentReadings(){
    	Reading temperatureReading = readingsService.getTemperatureReading();
        Reading humidityReading = readingsService.getHumidityReading();
        
        if(this.temperatureUnit.equals("K") && temperatureReading.getUnit().equals("C")) {
        	temperatureReading.setUnit("K");
        	double temperatureKelvin = Double.parseDouble(temperatureReading.getValue()) + 273.15;
        	
        	temperatureReading.setValue(String.valueOf(temperatureKelvin));
        }
        
        Set<Reading> response = new HashSet<>();
        response.add(temperatureReading);
        response.add(humidityReading);
        
        return ResponseEntity.ok().body(response);
    }


}
