package hr.fer.tel.rassus.lab3.aggregatormicroservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class ReadingService {

    @Autowired
    private RestTemplate restTemplate; 

    public Reading getTemperatureReading() {
        String url = "http://temperature-microservice/current-temperature";
        ResponseEntity<Reading> response = this.restTemplate.getForEntity(url, Reading.class);
        if(response.getStatusCode() == HttpStatus.OK){
            return response.getBody();
        }else{
            return null;
        }
    }
    
    public Reading getHumidityReading() {
        String url = "http://humidity-microservice/current-humidity";
        ResponseEntity<Reading> response = this.restTemplate.getForEntity(url, Reading.class);
        if(response.getStatusCode() == HttpStatus.OK){
            return response.getBody();
        }else{
            return null;
        }
    }
}
