package hr.fer.tel.rassus.lab3.temperaturemicroservice;

import java.io.FileReader;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class TemperatureMicroserviceApplication implements CommandLineRunner {

	@Autowired
	private TemperatureRepository temperatureRepository;

	public static void main(String[] args) {
		SpringApplication.run(TemperatureMicroserviceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		List<Reading> readings = new CsvToBeanBuilder<Reading>(new FileReader("../readings.csv"))
			       .withType(Reading.class).build().parse();

		long l = 0;

		for (Reading reading: readings) {
				String temperature = reading.getTemperature();

				TemperatureReading temperatureReading = new TemperatureReading(temperature);

				temperatureReading.setId(l++);

				temperatureRepository.save(temperatureReading);

				System.out.println(temperatureReading);
		}
	}

}
