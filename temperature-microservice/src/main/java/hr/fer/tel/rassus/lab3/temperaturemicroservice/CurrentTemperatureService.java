package hr.fer.tel.rassus.lab3.temperaturemicroservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrentTemperatureService {
    @Autowired
    private TemperatureRepository temperatureRepository;

    public TemperatureReading getCurrentTemperature() {
    	long id = (System.currentTimeMillis() % 100) + 1;
    	
        TemperatureReading temperatureReading = this.temperatureRepository.getById(id);
        
        return temperatureReading;
    }
}