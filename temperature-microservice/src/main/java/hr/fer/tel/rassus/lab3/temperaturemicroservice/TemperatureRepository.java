package hr.fer.tel.rassus.lab3.temperaturemicroservice;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TemperatureRepository extends JpaRepository<TemperatureReading, Long> {

}
