package hr.fer.tel.rassus.lab3.temperaturemicroservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class CurrentTemperatureController {
	CurrentTemperatureService currentTemperatureService;

    @Autowired
    public CurrentTemperatureController(CurrentTemperatureService currentTemperatureService) {
        this.currentTemperatureService = currentTemperatureService;
    }
    
    @GetMapping(value="/current-temperature")
    public ResponseEntity<TemperatureReading> getCurrentTemperature() {
    	TemperatureReading currentTemperature = this.currentTemperatureService.getCurrentTemperature();

        return currentTemperature != null ?
                ResponseEntity.ok().body(currentTemperature)
                	: ResponseEntity.noContent().build();
    }
}
