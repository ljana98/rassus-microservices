package hr.fer.tel.rassus.lab3.temperaturemicroservice;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class TemperatureReading {
    @Id
    private long id;
    
    private final String name = "Temperature";
    private final String unit = "C";
    
    private String value;

    public TemperatureReading(String value) {
        this.value = value;
    }
    
    public TemperatureReading() {
    	
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public String getName() {
    	return name;
    }
    
    public String getUnit() {
    	return unit;
    }
    
    public String getValue() {
    	return value;
    }
    
    public void setValue(String value) {
    	this.value = value;
    }

    @Override
    public String toString() {
        return "Temperature reading: [ "+
                "id: " + id + ", " +
                "name: " + name + ", " +
                "unit: " + unit + ", " +
                "value: " + value +
                " ]";
    }

}
